let ws;
let waiting = {};
let user;

function initWS() {
    ws = new WebSocket((location.protocol === "https:" ? "wss" : "ws") + "://" + location.host);
    ws.addEventListener("open", ev => {
        auth(document.cookie.split("=")[1]).then(data => {
            user = data.user;
        });
        document.getElementById("sendKC").addEventListener("submit", e => {
            e.preventDefault();
            let el = document.getElementById("kcText");
            sendText(el.value).then(data => {
                if (data.succ) {
                    /*document.getElementById("kc").innerHTML += `<div class="text">
                <div class="columns is-variable is-1-mobile is-0-tablet is-3-desktop is-8-widescreen is-2-fullhd">
                    <div class="column is-1">
                        <img src="${user.avatar}" width="32" height="32" />
                    </div>
                    <div class="column">
                        <span class="username">${user.username}</span>
                        <span class="tag is-success">Panel</span>
                        <div class="str">${el.value}</div>
                    </div>
                </div>
            </div>`;*/
                }
            });
        });
        
    });

    ws.addEventListener("message", ev => {
        let data = JSON.parse(ev.data);
        console.log(data);
        if (data.requestId && waiting[data.requestId]) {
            waiting[data.requestId](data);
            delete waiting[data.requestId];
        }
        if (data.op == 1) {
            if (data.from == "discord" || data.from == "panel") {
                document.getElementById("kc").innerHTML += `<div class="text">
            <div class="c1">
                <img src="${data.user.avatar}" width="32" height="32" />
                <div class="d">
                    <span class="username">${data.user.username}</span>
                    ${data.from == "panel" ? "<span class=\"panel\">PANEL</span>" : "<span class=\"discord\">DISCORD</span>"}
                    <div class="str">Real</div>
                </div>
            </div>
        </div>`;
            } else {
                // handle MC
            }
        }
    });
}

function auth(id) {
    return new Promise((resolve, reject) => {
        let reqId = Math.floor(Math.random() * 100000);
        ws.send(JSON.stringify({ op: 0, id, requestId: reqId }));
        waiting[reqId] = resolve;
    });
}

function sendText(str) {
    return new Promise((resolve, reject) => {
        let reqId = Math.floor(Math.random() * 100000);
        ws.send(JSON.stringify({ op: 1, m: str ?? "Not defined.", requestId: reqId }));
        waiting[reqId] = resolve;
    });
}

initWS();
