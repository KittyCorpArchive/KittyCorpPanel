const { GuildMember } = require("discord.js");
const KCRole = require("./KCRole");

class User {
    /**@param user {GuildMember} */
    constructor(user) {
        this.id = user.id;
        this.discriminator = user.user.discriminator;
        this.username = user.user.username;
        this.avatar = user.displayAvatarURL();
        this.displayColor = user.displayColor;
        this.tag = user.user.tag;
        this.accentColor = user.user.hexAccentColor;
        this.joinedAt = user.joinedAt;
        this.nickname = user.nickname;
        this.highestRole = new KCRole(user.roles.highest);
        this.roles = [];
        user.roles.cache.forEach(role => {
            this.roles.push(new KCRole(role));
        });
    }
}

module.exports = User;