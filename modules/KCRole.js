const { Role } = require("discord.js");

class KCRole {
    /**@param role {Role} */
    constructor(role) {
        this.id = role.id;
        this.name = role.name;
        this.managed = role.managed;
        this.position = role.position;
        this.color = role.hexColor;
    }
}

module.exports = KCRole;