const mysql = require("mysql");


class Database {
    static client = null;
    static init() {
        this.client = mysql.createConnection({
            host: "localhost",
            user: "kc",
            port: 5123,
            database: "kittycorp" // we were bothered to change this
        });
    }

    static getPlayer(key) {
        return new Promise((resolve, reject) => {
            this.client.query("SELECT * FROM players WHERE username = ? OR uuid = ?", [key, key], (err, result) => {
                resolve(result);
            });
        });
    }

}

module.exports = Database;
