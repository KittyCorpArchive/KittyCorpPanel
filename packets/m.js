module.exports = {
    id: 1,
    auth: true,
    execute: function (packet, client, req, handler) {
        const { data, clients, kittycorpBots, webhookClient } = handler;
        let user = clients.get(client);
        clients.forEach((u, c) => {
            if (client == c) return;
            c.send(JSON.stringify({ op: 1, user, from: "panel", m: packet.m }));
        });
        client.send(JSON.stringify({requestId: packet.requestId, succ: 1}));
        webhookClient.send({
            content: packet.m.replace(/@/g, "@\u200B\u200B"),
            allowed_mentions: { parse: [] },
            username: `${user.tag} (PANEL)`,
            avatarURL: user.avatar
        });
        for (let [key, client] of Object.entries(kittycorpBots)) {
            client.send(JSON.stringify({ op: 0, sender: user.tag, serverName: "PANEL", server: "PANEL", m: packet.m, type: 0 }));
        }
    }
}
