module.exports = {
    id: 0,
    auth: false,
    execute: function (packet, client, req, handler) {
        const {data, clients, players} = handler;
        if (data[packet.id] != null) {
            client.send(JSON.stringify({ op: 0, succ: 1, user: data[packet.id], requestId: packet.requestId, players }));
            clients.set(client, data[packet.id]);
            return;
        } else {
            client.send(JSON.stringify({ op: 0, succ: 0, user: null, requestId: packet.requestId, reason: "Invalid ID." }));
        }
    }
}