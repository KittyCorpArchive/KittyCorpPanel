const fs = require("fs");


class CommandHandler {
  constructor (dir = "./packets", data={}) {
    this.packets = {};
    Object.entries(data).forEach(([key, value]) => {
        this[key] = value;
    });
    this.loadPackets(dir);
  }

  execute (id, ...params) {
    const packet = this.packets[id];
    if (packet != null) {
      try {
        packet.execute(...params, this);
      } catch (e) {
        console.log(`Error while executing packet: ${packet.id}:`, e);
      }
    }
  }

  findPacket(id) {
    return this.packets[id];
  }

  loadPackets(dir) {
    for (const packetFile of fs.readdirSync(dir)) {
      const path = dir + "/" + packetFile;
      try {
        const packet = require(path);
        this.packets[packet.id] = packet;
      } catch (e) {
        console.log("Error with packet file: " + packetFile, e);
      }
    }
  }
}

module.exports = CommandHandler;
