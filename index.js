const http = require("http");
const WebSocket = require("ws");
const config = require("./config.json");
const express = require("express");
const bodyParser = require("body-parser");
const cookieParser = require("cookie-parser");
const { EmbedBuilder, WebhookClient, Client, GatewayIntentBits, AuditLogOptionsType } = require('discord.js');
const {data, clients} = require("./data");
const User = require("./modules/User");
const PacketHandler = require("./PacketHandler");
const Database = require("./Database");

if (!config.hasOwnProperty("token")) {
    console.log("Warning: The field \"token\" is missing in the config file!");
}

Database.init();
const dclient = new Client({
    intents: [
        GatewayIntentBits.Guilds,
        GatewayIntentBits.GuildMessages,
        GatewayIntentBits.MessageContent,
        GatewayIntentBits.GuildMembers,
    ],
});

const webhookClient = new WebhookClient({ id: config.webhookId, token: config.webhookToken });
const app = express();


process.on("uncaughtException", console.log);

app.set("etag", false);
app.set("view engine", "ejs");

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

app.use((req, res, next) => {
    req.client = dclient;
    next();
});

app.use("/", require("./route/root"));
app.use("/api", require("./route/api"));


let httpServer = http.createServer(app);
let server = new WebSocket.WebSocketServer({ noServer: true, clientTracking: false });

let kittycorpBots = {};
let players = {};
const botPacketHandler = new PacketHandler("./botpackets", {clients, kittycorpBots, webhookClient, players});
const packetHandler = new PacketHandler("./packets", {data, clients, players, kittycorpBots, webhookClient});


dclient.on("messageCreate", msg => {
    if (msg.author.bot) { return };
    if (msg.channel.id === config.channelID) {
        for (let [key, client] of Object.entries(kittycorpBots)) {
            client.send(JSON.stringify({ op: 0, sender: msg.member.nickname ?? msg.author.tag, serverName: "DISCORD", server: "DISCORD", m: msg.content, type: 0 }));
        }
        let user = new User(msg.member);
        clients.forEach((u, c) => {
            c.send(JSON.stringify({ op: 1, user, m: msg.content, from: "discord" }));
        });
    }
});

/**@param client {WebSocket.WebSocket} */
function initListeners(client, req) {
    client.on("message", data => {
        try {
            data = JSON.parse(data);
            if (typeof data.op != "number") return;
            botPacketHandler.execute(data.op, data, client);
        } catch (e) {
            console.log("kittycorp bot sent invalid packet", e);
        }
    });
}

server.on("kConnect", (client, req) => {
    client.server = req.headers.server;
    client.serverName = req.headers.servername;
    kittycorpBots[req.headers.server] = client;
    initListeners(client);
});

/**@param client {WebSocket.WebSocket} @param req {http.IncomingMessage} */

function handle(client, req) {
    client.on("message", packet => {
        try {
            packet = JSON.parse(packet);
            if (typeof packet.op != "number") return;
            let p = packetHandler.findPacket(packet.op);
            if (p) {
                if (p.auth && !clients.get(client)) return console.log("not authed.");
                packetHandler.execute(packet.op, packet, client, req);
            }
        } catch (e) {
            console.log("Error while trying to parse packet from client:", e);
        }
    });
}

server.on("pConnect", handle);

httpServer.on("upgrade", (req, socket, head) => {

    if (req.url == "/kittycorpBots") {
        if (config.token && !req.headers.authorization || req.headers.authorization != config.token) {
            socket.write("HTTP/1.1 401 Unauthorized\r\n\r\n");
            socket.destroy();
            return;
        }
        server.handleUpgrade(req, socket, head, (client, req) => {
            server.emit("kConnect", client, req);
        });
    }

    else if (["/panel", "/webpanel", "/"].includes(req.url.toLowerCase())) {
        server.handleUpgrade(req, socket, head, (client, req) => {
            server.emit("pConnect", client, req);
        });
    }

    else {
        socket.write("HTTP/1.1 404 Not Found\r\n\r\n");
        socket.destroy();
    }

});


httpServer.listen(6060);
dclient.login(config.discordToken);