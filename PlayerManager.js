class Player {
    constructor(player, server, online=false) {
        this.uuid = player.uuid;
        this.username = player.username;
        this.ping = player.ping;
        this.gamemode = player.gamemode;
        this.server = server;
        this.online = online;
    }
    
}

class PlayerManager {

    static playersByName = {};
    static playersByUUID = {};
    static playersByServer = {};

    static getPlayerByName(name) {
        return this.playersByName[name] ?? null;
    }

    static getPlayerByUUID(uuid) {
        return this.playersByUUID[uuid] ?? null;
    }

    static getPlayer(key) {
        return (this.playersByUUID[key] ?? this.playersByName[key]) ?? null;
    }

    static getPlayersByServer(server) {
        return this.playersByServer[server];
    }

    static addPlayer(player, server, online=false) {
        let p = new Player(player, server, online);
        this.playersByName[player.username] = p;
        this.playersByUUID[player.uuid] = p;
        if (this.playersByServer[server] == null)
            this.playersByServer[server] = {};
        this.playersByServer[server][player.username] = p;
    }

    static removePlayer(player, server) {
        delete this.playersByName[player.username];
        delete this.playersByUUID[player.uuid];
        delete this.playersByServer[server][player.username];
    }

}

module.exports = PlayerManager;