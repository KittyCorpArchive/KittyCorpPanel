const Router = require("express").Router;
const OAuth = require("../OAuth");
const crypto = require("crypto");
const config = require("../config.json");
const {data, clients} = require("../data");
const User = require("../modules/User");
const PlayerManager = require("../PlayerManager");
const Database = require("../Database");

let router = Router();
const oauthClient = new OAuth(config.client_id, config.secret, config.redirectUri ?? "https://kc.ginlang.xyz/api/discord/callback"); // probably wanna make it configurable


router.get("/discord/callback", async (req, res) => {
    let resp = await oauthClient.exchangeCode(req.query.code);
    let user = await oauthClient.getUser(resp.access_token);
    let id = crypto.randomBytes(13).toString("hex");
    let userObj = (await req.client.guilds.cache.at(0).members.fetch()).get(user.id);

    if (!userObj) return res.send("Ig not in the discord server.,.,.");
    if (!userObj.roles.cache.get("1025836835735351296")) {
        return res.send("I guess you don't have the super secret role! Ig no panel for you.,.,.,...,.,.");
    }
    
    data[id] = new User(userObj);
    res.cookie("sessionID", id);
    res.redirect("/");
});

router.get("/list", (req, res) => {
    let obj = {};
    if (!req.query.server) {
        Object.keys(PlayerManager.playersByServer).forEach(serverName => {
            obj[serverName] = {};
            let players = PlayerManager.playersByServer[serverName];
            Object.keys(players).forEach(playerName => {
                obj[serverName][playerName] = players[playerName];
            });
        });
        return res.header("Content-Type", "application/json").send(JSON.stringify(obj, null, 4));
    }
});


router.get("/lastSeen", (req, res) => {
    if (!req.query.username && !req.query.uuid)
        return res.send({success: false, reason: "Please provide a \"username\" or \"uuid\" query parameter"});
    let player;

    if (req.query.username) {
        player = PlayerManager.getPlayerByName(req.query.username);
    }

    if (req.query.uuid) {
        player = PlayerManager.getPlayerByUUID(req.query.uuid);
    }

    if (player != null) {
        return res.send({
            success: true,
            username: player.username,
            uuid: player.username,
            lastSeen: Date.now(),
            online: true,
            server: player.server
        });
    } else {
        Database.getPlayer(req.query.uuid ?? req.query.username).then(p => {
            if (!p || p.length < 1)
                return res.send({success: false, err: "PLAYER_NOT_FOUND"});
            return res.send({
                success: true,
                username: p[0].username,
                uuid: p[0].uuid,
                lastSeen: p[0].lastSeen,
                server: p[0].server,
                online: false
            });
        });
    }

});

module.exports = router;