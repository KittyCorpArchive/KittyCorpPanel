const Router = require("express").Router;
const {data, clients} = require("../data");
const config = require("../config.json");

let router = Router();

router.get("/", (req, res) => {
    if (data[req.cookies.sessionID] != null) {
        req.user = data[req.cookies.sessionID];
    } else {
        res.redirect("/login");
        return;
    }
    res.render("index", {req, res});
});

router.get("/login", (req, res) => {
    if (data[req.cookies.sessionID] != null) {
        return res.redirect("/");
    }
    return res.redirect("https://discord.com/api/oauth2/authorize?client_id=1038103862197878896&redirect_uri=" + (config.redirectUri ? encodeURIComponent(config.redirectUri) : "https%3A%2F%2Fkc.ginlang.xyz%2Fapi%2Fdiscord%2Fcallback") + "&response_type=code&scope=identify");
});

router.use(require("express").static(__dirname + "/../client"));




module.exports = router;