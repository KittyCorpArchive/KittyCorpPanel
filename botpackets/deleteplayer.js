const PlayerManager = require("../PlayerManager")

module.exports = {
    id: 2,
    execute: function (data, client, handler) {
        PlayerManager.removePlayer(data.player, client.server);
    }
}
