const PlayerManager = require("../PlayerManager");

module.exports = {
    id: 3,
    execute: function (data, client, handler) {
        client.send(JSON.stringify({ op: 3, player: PlayerManager.getPlayer(data.player), requestId: data.requestId }));
    }
}