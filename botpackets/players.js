const PlayerManager = require("../PlayerManager");

module.exports = {
    id: 1,
    execute: function (data, client, handler) {
        Object.entries(data.players).forEach(([key, player]) => {
            PlayerManager.addPlayer(player, client.server, true);
        });
    }
}
