const ChatMessage = require("prismarine-chat")("1.18.2");

module.exports = {
    id: 0,
    execute: function (data, client, handler) {
        const toSend = new ChatMessage(data.m).toString();
        const {clients, webhookClient, kittycorpBots} = handler;
        for (let [key, c] of Object.entries(kittycorpBots)) {
            if (key != client.server) {
                c.send(JSON.stringify({ op: 0, ...data, server: client.server, serverName: client.serverName }));
            }
        }
        if (data.type == 0) {
            webhookClient.send({
                content: toSend.replace(/@/g, "@\u200B\u200B"), allowed_mentions: { parse: [] },
                username: data.sender + ` (${client.serverName})`,
                avatarURL: `https://cataas.com/cat/2LC9Ne6SIMXnIdXZ`
            });
            clients.forEach((u, c) => {
                c.send(JSON.stringify({ op: 1, sn: client.serverName, sender: data.sender, m: toSend, from: "mc" }));
            });
        } else {
            webhookClient.send({
                content: toSend.replace(/@/g, "@\u200B\u200B"), allowed_mentions: { parse: [] },
                username: "KittyCorp™️" + ` (${client.serverName})`,
                avatarURL: `https://cataas.com/cat/iD7RU5SKBkeQijJn`
            });
        };
    }
}