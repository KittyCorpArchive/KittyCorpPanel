const { fetch } = require("undici");
const qs = require("querystring");

const BASE_URL = "https://discord.com/api/v10"
const OAUTH_BASE_URL = BASE_URL + "/oauth2";

class OAuth {
    constructor(clientId, clientSecret, redirectUri) {
        this.clientId = clientId;
        this.clientSecret = clientSecret;
        this.redirectUri = redirectUri;
    }

    async exchangeCode(code) {
        let res = await fetch(`${OAUTH_BASE_URL}/token`, {
            method: "POST",
            headers: {
                "Content-Type": "application/x-www-form-urlencoded"
            },
            body: qs.stringify({
                client_id: this.clientId,
                client_secret: this.clientSecret,
                grant_type: "authorization_code",
                code,
                redirect_uri: this.redirectUri
            })
        });
        let json = await res.json();
        return json;
    }

    async getUser(token) {
        let res = await fetch(`${BASE_URL}/users/@me`, {
            method: "GET",
            headers: {
                "Authorization": `Bearer ${token}`
            },
        });
        let json = await res.json();
        return json;
    }
    
}

module.exports = OAuth;